import React, { Component } from 'react';

import like from '../assets/images/like.png';
import facebook from '../assets/images/facebook.png';
import twitter from '../assets/images/twitter.png';
import whatsapp from '../assets/images/whatsapp.png';

class productDetail extends Component {

    render() {
    return (
        <div className="productList--product">
            <div className="product-image">
                <img src={this.props.product.otherimages} alt={this.props.product.productname} title={this.props.product.productname}/>
            </div>
            <div className="product-information">
                <h2 className="brand">{this.props.product.brand}</h2>
                <h3 className="title">{this.props.product.productname}</h3>
                <div className="seller">Vendido y entregado: <span>{this.props.product.brand}</span></div>
                <a href="javascript:;" className="whishlist"><img src={like}  alt={this.props.product.productname}/></a>
            </div>
            <div className="product-sizes">
                <div className="title">Talle</div>
                <div className="sizes-list">
                {
                    this.props.product.sizes.map(size => (
                        <div className="item" key={size.id} >{size.size}</div>
                    ))
                    
                }
                </div>
            </div>
            <div className="product-social">
                <ul className="list-social">
                    <li className="social-item">
                        <a href="" className="social-link"><img src={facebook}  alt="Facebook"/></a>
                    </li>
                    <li className="social-item">
                        <a href="" className="social-link"><img src={twitter}  alt="Twitter"/></a>
                    </li>
                    <li className="social-item">
                        <a href="" className="social-link"><img src={whatsapp}  alt="Whastapp"/></a>
                    </li>
                </ul>
            </div>
            <div className="product-tabs">
                <div className="tabs">
                <div className="title">Descripción</div>
                    <p>{this.props.product.description}</p>
                </div>
            </div>
        </div>
    );
  }
}

export default productDetail;
