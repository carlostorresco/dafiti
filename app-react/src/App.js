import React, { Component } from 'react';
//import './App.css';

import ProductList from './components/ProductList.jsx';
import products from './detalle.json';


class App extends Component {
  constructor(props) {
		super(props)
		this.state = { 
            products: products.products
        }
	}
    render() {
        return (
            <>  
                <main>
                    <ProductList products={this.state.products} />
                </main>
            </>
        );
    }
}

export default App;
