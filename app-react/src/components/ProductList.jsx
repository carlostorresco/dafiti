import React, { Component } from 'react';

import ProductDetail from '../components/ProductDetail.jsx';
import "../assets/sass/app.scss";


class ProductList extends Component {
    render() {
        return (
        <>
            <section className="productList">
                <h2 className="productList--title">Dafiti React Test</h2>
            {
                this.props.products.map(product => (
                    <ProductDetail key={product.id} product={product} />
                ))
            }
            </section>
        </>
    );
    }
}

export default ProductList;
