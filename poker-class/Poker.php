<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

class Poker {

    public static function isStraight($cards){

        $counter = 1;
        sort($cards);
        
        if(count($cards) < 5 || count($cards) > 7){
            return false;
        }
        
        if(end($cards) == 14 && reset($cards) == '2'){
            array_unshift($cards, 1);
        }

        for($i=1;$i<=count($cards);$i++) {      
            if($cards[$i-1]+1 == $cards[$i]) {
                $counter++;
                if ($counter == 5) {
                    return true;
                }
            }
            else {
                return false;
            }
        }
        
    }
}
?>